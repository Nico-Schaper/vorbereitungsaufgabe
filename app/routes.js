// app/routes.js

// grab the nerd model we just created
var Nerd = require('./models/nerd');

module.exports = function(app) {


//					Get all IDs


	app.get('/api/user', function(req, res) {
		Nerd.find({}, {password: 0}, function(err, nerds) {
			if (err)
				res.send(err);
			res.json(nerds); 
		});
	});


//					Get specific ID


app.get('/api/user/:name', function(req, res) {
	Nerd.find({where:{name:req.params.name}}, function(err, nerds) {
		if (err)
			res.send(err);
		res.json(nerds);
	});
});


//					CREATE


app.post('/api/user', function(req, res){
	Nerd.create({
		name: 		req.body.name,
		surname: 	req.body.surname,
		username: 	req.body.username,
		password: 	req.body.password,
		done: false

	}, function(err, nerd){
		if(err){
			res.send(err)
		}
		Nerd.find({}, {password: 0},function(err, nerds){
			if(err){
				res.send(err);
			}

			res.json(nerds);
		});
	});
});


//				DELETE


app.delete('/api/user/:name', function(req, res) {
	Nerd.remove({
		name : req.params.name
	}, function(err, nerd) {
		if (err)
			res.send(err);
		
		Nerd.find({}, {password: 0},function(err, nerds) {
			if (err)
				res.send(err)
			res.json(nerds);
		});
	});
});


//				UPDATE


app.put('/api/user/:name', function(req, res){
	Nerd.update({
		name: req.params.name
	}, {
		name: 		req.body.name,
		surname: 	req.body.surname,
		username: 	req.body.username,
		done: false

	},
	function(err){
		if (err){
			res.send(err);
		};

		Nerd.find({}, {password: 0},function(err, nerds) {
			if (err){
				res.send(err);
			}
			res.json(nerds);
		});
	});
});


//					UMLEITUNG


app.get('/*', function(req, res) {
		res.sendfile('./public/views/index.html'); // load our public/index.html file
	});
};