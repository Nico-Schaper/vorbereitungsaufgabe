// public/js/appRoutes.js
angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

// home page
.when('/', {
	templateUrl: 'views/home.html',
	controller: 'MainController'
})

// nerds page that will use the NerdController
.when('/nerds', {
	templateUrl: 'views/nerd.html',
	controller: 'NerdController'
})

.when('/userlist', {
	templateUrl: 'views/userlist.html',
	controller: 'UserListController'
})

.when('/nerds/newUser', {
	templateUrl: 'views/nerds/newUser.html',
	controller: 'NerdController'
})
.when('/nerds/:nerd_id', {
	templateUrl: 'views/nerd.html',
	controller: 'NerdController'
})

.otherwise({redirectTo:'/'});

$locationProvider.html5Mode(true);

}]);

