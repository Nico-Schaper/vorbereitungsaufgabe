// public/js/controllers/MainCtrl.js
angular.module('NerdCtrl', []).controller('NerdController', function($scope, Nerd) {
	$scope.isEditing = false;
	$scope.deleted = false;
	var count = 0, advancedSearchBool = false;
	Nerd.get(function(nerdData){
		$scope.nerds 	= nerdData;
		console.log($scope.nerds);
	});

	$scope.advancedSearch	= function(){
		count++;
		if(count == 2){
			$scope.searchState = "";
			count = 0;
			advancedSearchBool = false;
		}else{
			$scope.searchState = "Cancel";
			advancedSearchBool = true;
		}
	}

	$scope.sorting 		= function(){
		if(!$scope.sortOrder){
			$scope.ordered = "ascending";
			$scope.sortOrder = true;
		} else {
			$scope.ordered = "descending";
			$scope.sortOrder = false;
		}
	} 
	$scope.createNerd = function(data){
		Nerd.create(data, function(nerds){
			$scope.nerds = nerds;
			$scope.formData = null;
		});
	}
	$scope.deleteNerd	 = function(name){
		var confirmation = confirm('Are you sure you want to delete this user?');
		if(confirmation){
			var deletedName = name;
			Nerd.delete(name, function(data){
				$scope.nerds = data;
				$scope.deletedName = deletedName;
				$scope.deleted = true;
			});
		}
		
	}
	$scope.startEdit = function (eData){
		count++;
		if(count == 2){
			$scope.cancelEdit = true;
			count = 0;
			$scope.isEditing = false;
		}else{
			$scope.cancelEdit = false;
			$scope.isEditing = true;
		}
		$scope.updateData = angular.copy(eData);
	}
	$scope.putNerd		= function(name){
		Nerd.edit(name, $scope.updateData, function(data){
			$scope.nerds = data;
		});
		$scope.isEditing = false;
	}
});
