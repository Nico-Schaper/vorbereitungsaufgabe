// public/js/services/NerdService.js
angular.module('NerdService', []).service('Nerd', ['$http', function($http) {

	return {
		// call to get all nerds
		get : function(callback) {
			 $http.get('/api/user').success(function(data){
				callback(data);
			});
		},

		create : function(nerdData, callback) {
			 $http.post('/api/user', nerdData).success(function(data){
				callback(data);
			});
		},

		delete : function(name, callback) {
			 $http.delete('/api/user/' + name).success(function(data){
				callback(data);
			});
		},

		edit : function(name, updateData, callback){
			$http.put('/api/user/' + name, updateData).success(function(data){
				callback(data);
			});
		}
	}       

}]);

